import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {


  form!:FormGroup;
  arrayutiles:string[]=[];
  constructor(private fb:FormBuilder) { 
    this.cargarForm();
    
  }

  ngOnInit(): void {
  }

  get invalido(){
    return this.form.invalid;
  }

  get getUtiles(){
    
    return this.form.get('utiles') as FormArray;
  }
  
   
  cargarForm(){
   
    this.form = this.fb.group({      
       utiles: this.fb.array([])
    })
    this.getUtiles.push(this.fb.group({
      check: [null],
      utilNuevo : [null,Validators.pattern(/^[1-9-0-a-zA-zñÑ\s]+$/)]
    }));
  }

  //En conjunto:
  eliminaTodo(){
    this.arrayutiles=[];
    this.getUtiles.clear()
  }


  Adiciona():void{
    const nuevo = this.fb.group({
      check: [null],
      utilNuevo : [null,Validators.pattern(/^[0-9a-zA-zñÑ\s]+$/)]
    })
    this.getUtiles.insert(0,nuevo);
  }

  deleteUtilEscolar(id:number){
    this.getUtiles.removeAt(id);
  }


  guardar(){
    this.arrayutiles=[];
    for (let i = 0; i < this.getUtiles.length; i++) {
      if (this.getUtiles.at(i).get('check')?.value ==true &&  this.getUtiles.at(i).get('utilNuevo')?.value != null ) {
        this.arrayutiles.push( this.getUtiles.at(i).get('utilNuevo')?.value);
      }  
    }
    console.log(this.arrayutiles);
    
    //  this.arrayutiles = this.form.value.utiles;
    //  console.log(this.getUtiles.at(0).get('check')?.value);
    //  console.log(this.getUtiles.at(0).get('utilNuevo')?.value);
     
  }

  limpiar():void{
    for (let i = 0; i < this.getUtiles.length; i++) {
       this.getUtiles.at(i).get('utilNuevo')?.setValue(null);
       this.getUtiles.at(i).get('check')?.setValue(null);
       
    }
    this.form.get('utiles')?.reset;
  }



}
